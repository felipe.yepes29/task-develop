<?php

namespace AppBundle\Controller;

use AppBundle\Services\Helpers;//Llamamos el servicio de helpers
use AppBundle\Services\JwtAuth;//Llamamos el servicio de autenticacion
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;//Para generar respuestas en Json
use Symfony\Component\Validator\Constraints as Assert; // as para poner el alias, libreria para validar de symfony

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    public function loginAction(Request $request){
        $helpers = $this->get(Helpers::class);

        // Recibir json por POST
        $json = $request->get('json', null);

        // Array a devolver por defecto
        $data = array(
            'status' => 'error',
            'data' => 'Send json via post !!'
        );

        if ($json != null){
            //Me haces el login

            // Convertimos un json a un objeto de PHP
            $params = json_decode($json);

            $email = (isset($params->email)) ? $params->email : null;
            $password = (isset($params->password)) ? $params->password : null;
            $getHash = (isset($params->getHash)) ? $params->getHash : null;

            // Validamos con el alias de la libreria de validación de symfony
            $emailConstraint = new Assert\Email();
            $emailConstraint->message = ('El correo no es válido !!');
            $validate_email = $this->get('validator')->validate($email, $emailConstraint);

            if ($email != null && count($validate_email) == 0 && $password != null){

                $jwt_auth = $this->get(JwtAuth::class);

                if ($getHash == null || $getHash == false){
                    $signup = $jwt_auth->signup($email, $password);
                }else{
                    $signup = $jwt_auth->signup($email, $password, true);//nos trae todos los datos decodificados
                }

                return $this->json($signup);

            }else{
                $data = array(
                    'status' => 'error',
                    'data' => 'Email or password incorrect'
                );
            }
        }

        return $helpers->json($data);
    }

    public function pruebasAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository('BackendBundle:User');
        $users = $userRepo->findAll();

        $helpers = $this->get(Helpers::class);
        return $helpers->json(array(
            'status' => 'success',
            'users' => $users
        ));

    }

}
