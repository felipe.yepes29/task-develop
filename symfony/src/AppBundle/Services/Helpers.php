<?php

namespace AppBundle\Services;

class Helpers{

    public $manager;

    public function __construct($manager)//Le pasamos el parametro manager del servicio
    {
        $this->manager = $manager;
    }

    public function json($data){
        $normalizers = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
        $encoders = array("Json" => new \Symfony\Component\Serializer\Encoder\JsonEncoder());

        $serializer = new \Symfony\Component\Serializer\Serializer($normalizers, $encoders);
        $json = $serializer->serialize($data, 'json');

        $reponse = new \Symfony\Component\HttpFoundation\Response();
        $reponse->setContent($json);
        $reponse->headers->set('Content-Type', 'application/json');

        return $reponse;
    }
}