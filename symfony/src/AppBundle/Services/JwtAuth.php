<?php

namespace AppBundle\Services;

use BackendBundle\BackendBundle;
use BackendBundle\Entity\User;
use Firebase\JWT\JWT;

class JwtAuth
{

    public $manager;
    public $key;

    public function __construct($manager)
    {//Le pasamos el parametro manager del servicio
        $this->manager = $manager;
        $this->key = 'Hola_soy-la_clave_secreta34572q31442472457089.,';
    }

    public function signup($email, $password, $getHash = null)
    {

        $user = $this->manager->getRepository('BackendBundle:User')->findOneBy(array(
            'email' => $email,
            'password' => $password
        ));

        $signup = false;

        if (is_object($user)) {
            $signup = true;
        }

        if ($signup == true) {
            //Generar un toque JWT
            $token = array(
                'sub' => $user->getId(),
                'email' => $user->getEmail(),
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
                'iat' => time(),
                'exp' => time() + (7 * 24 * 60 * 60)
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, array(
                'HS256'
            ));

            if ($getHash == null) {
                $data = $jwt;
            } else {
                $data = $decoded;
            }

        } else {
            $data = array(
                'status' => 'error',
                'data' => 'Login failed !!'
            );
        }

        return $data;
    }

}